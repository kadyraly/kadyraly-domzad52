import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Number from "./Number/Number";

class App extends Component {

  state = {
    number: [0, 0, 0, 0, 0]
  };

  changeNumbers = () => {
    let newNumbers = [];
    let count = 5;
    while (count > 0) {
      let getRandom = Math.floor(Math.random()*36) + 1
      if (newNumbers.indexOf(getRandom ) === -1) {
        newNumbers.push(getRandom);
        count--
      }
    } this.setState({number: newNumbers});
    newNumbers.sort(function(a, b) {
      return a - b;
    });
  }

  render() {
    return (
      <div className="App">
        <button onClick={this.changeNumbers}> Change Numbers</button>
        <Number numb={this.state.number[0]} />
        <Number numb={this.state.number[1]} />
        <Number numb={this.state.number[2]} />
        <Number numb={this.state.number[3]} />
        <Number numb={this.state.number[4]} />
      </div>
    );
  }
}

export default App;
